# Descomplicando o Gitlab

Treinamento ao vivo - Descomplicando o Gitlab na Twitch

### Day-1

```bash
- Entendemos o que é o Git
- Entendemos o que é o Gitlab
- Como criar um grupo no Gitlab
- Como criar um repositorio Git
- Aprendemos os comandos básicos para manipulação de arquivos e diretórios no git
- Como criar uma branch
- Como criar um Merge Request
- Como adicionar um membro no projeto
- Como fazer o merge na Master/Main
- Como associar um repo local com um repo remoto
- Como importar um repo do Github para o Gitlab
- Mudamos a branch padrão para Main
```
